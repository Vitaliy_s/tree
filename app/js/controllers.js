'use strict';

/* Controllers */

angular.module('myApp.controllers', [])

.controller("TreeController", ['$scope', function ($scope) {

    $scope.isCheckedShown = true;

    // default tree tasks model
    $scope.tasksTree = {
        name: "project",
        checked: false,
        children: [
            {name: "task0", checked: false, children: []},
            {name: "task1", checked: false, children: []},
            {name: "task2", checked: false, children: [
                {name: "task0", checked: false, children: []},
                {name: "task1", checked: false, children: [
                    {name: "task0", checked: false, children: []},
                    {name: "task1", checked: false, children: []},
                    {name: "task2", checked: false, children: []}
                ]},
                {name: "task2", checked: false, children: []}
            ]}
        ]
    };
}]);
