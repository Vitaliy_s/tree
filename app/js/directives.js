'use strict';

/* Directives */


angular.module('myApp.directives', [])

.directive('treeNode', function ($compile) {

    /**
     * Add node to the tree
     * @param treeNode
     */
    var addTreeNode = function (treeNode) {
        treeNode.children.push({
            name: 'task' + parseInt(treeNode.children.length + 1),
            checked: false,
            children: []
        });
    };

    /**
     * Remove node from tree
     * @param parentNode
     * @param index
     */
    var removeTreeNode = function (parentNode, index) {
        if (parentNode) {
            parentNode.children.splice(index, 1);
        }
    };

    return {
        restrict: "A",
        replace: true,
        scope: {
            parentNode: "=",
            treeNode: "=",
            treeNodeIndex: "=",
            isShown: "="
        },
        template:
            '<li data-ng-hide="treeNode.checked && !isShown">' +
                '<label>' +
                    '<input type="checkbox" data-ng-model="treeNode.checked">{{ treeNode.name }}' +
                    '<button data-ng-click="addTreeNode(treeNode)">+</button>' +
                    '<button data-ng-show="parentNode" data-ng-click="removeTreeNode(parentNode, treeNodeIndex)">' +
                        '-' +
                    '</button>' +
                '</label>' +
            '</li>',

        link: function (scope, element) {

            scope.addTreeNode = addTreeNode;
            scope.removeTreeNode = removeTreeNode;

            var $nodes = $compile(
                '<ul>' +
                    '<li data-ng-repeat="node in treeNode.children" data-is-shown="isShown"' +
                        'data-parent-node="treeNode" data-tree-node="node" data-tree-node-index="$index">' +
                    '</li>' +
                '</ul>'
            )(scope);

            element.append($nodes);
        }
    }
});

