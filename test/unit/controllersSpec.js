'use strict';

/* jasmine specs for controllers go here */

describe('controllers', function(){

    var $scope;

    var mockTasksTree = {
        name: "project",
        checked: false,
        children: [
            {name: "task0", checked: false, children: []},
            {name: "task1", checked: false, children: []},
            {name: "task2", checked: false, children: [
                {name: "task0", checked: false, children: []},
                {name: "task1", checked: false, children: [
                    {name: "task0", checked: false, children: []},
                    {name: "task1", checked: false, children: []},
                    {name: "task2", checked: false, children: []}
                ]},
                {name: "task2", checked: false, children: []}
            ]}
        ]
    };

    beforeEach(module('myApp.controllers'));

    beforeEach(inject(function ($rootScope, $controller) {
        $scope = $rootScope.$new();

        $controller('TreeController', {
            $scope: $scope
        });
    }));

    describe('TreeController', function () {

        it('should have required properties in scope', function () {
            expect($scope.isCheckedShown).toBeDefined();
            expect($scope.tasksTree).toBeDefined();
        });

        it('should have the tasksTree property and it should be equal to the mock data above', function () {
            expect($scope.tasksTree).toEqual(mockTasksTree);
        });
    });

});
