'use strict';

/* jasmine specs for directives go here */

describe('directives', function() {

    var scope, elm, $compile;

    beforeEach(module('myApp.directives'));

    beforeEach(inject(function(_$rootScope_, _$compile_) {
        $compile = _$compile_;
        scope = _$rootScope_;

        elm = angular.element('<ul><li data-tree-node="tasksTree" data-is-shown="isCheckedShown"></li></ul>');

        scope.tasksTree = {
            name: "project",
            checked: false,
            children: [
                {name: "task0", checked: false, children: []},
                {name: "task1", checked: false, children: []}
            ]
        };

        scope.isCheckedShown = true;

        $compile(elm)(scope);
        scope.$digest();
    }));

    describe('treeNode directive', function () {

        it('should have nodes in DOM after compiling', function () {
            expect(elm.find('li').length).toBe(3);
        });

        it('should add task to tree', function () {
            expect(elm.find('li').length).toBe(3);

            elm.find('li:eq(0) button:eq(0)').click();
            scope.$digest();

            expect(elm.find('li').length).toBe(4);
            expect(scope.tasksTree.children.length).toBe(3);
        });

        it('should remove task from tree', function () {
            expect(elm.find('li').length).toBe(3);

            elm.find('li:eq(1) button:eq(1)').click();
            scope.$digest();

            expect(elm.find('li').length).toBe(2);
            expect(scope.tasksTree.children.length).toBe(1);
        });

        it('should check/uncheck checkbox at the node', function () {
            elm.find('li:eq(0) input').click();
            scope.$digest();

            expect(scope.tasksTree.checked).toBeTruthy();

            elm.find('li:eq(0) input').click();
            scope.$digest();

            expect(scope.tasksTree.checked).toBeFalsy();
        });

        it('should show the nodes with unchecked checkboxes only', function () {
            elm.find('li:eq(1) input').click();
            scope.isCheckedShown = false;
            scope.$digest();

            expect(scope.tasksTree.children[0].checked).toBeTruthy();
            expect(elm.find('li:eq(1)').hasClass('ng-hide')).toBeTruthy();
        });
    });
});
